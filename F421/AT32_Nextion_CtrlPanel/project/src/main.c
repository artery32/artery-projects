/* add user code begin Header */
/**
 **************************************************************************
 * @file     main.c
 * @brief    main program
 **************************************************************************
 *                       Copyright notice & Disclaimer
 *
 * The software Board Support Package (BSP) that is made available to
 * download from Artery official website is the copyrighted work of Artery.
 * Artery authorizes customers to use, copy, and distribute the BSP
 * software and its related documentation for the purpose of design and
 * development in conjunction with Artery microcontrollers. Use of the
 * software is governed by this copyright notice and the following disclaimer.
 *
 * THIS SOFTWARE IS PROVIDED ON "AS IS" BASIS WITHOUT WARRANTIES,
 * GUARANTEES OR REPRESENTATIONS OF ANY KIND. ARTERY EXPRESSLY DISCLAIMS,
 * TO THE FULLEST EXTENT PERMITTED BY LAW, ALL EXPRESS, IMPLIED OR
 * STATUTORY OR OTHER WARRANTIES, GUARANTEES OR REPRESENTATIONS,
 * INCLUDING BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.
 *
 **************************************************************************
 */
/* add user code end Header */

/* Includes ------------------------------------------------------------------*/
#include "at32f421_wk_config.h"

/* private includes ----------------------------------------------------------*/
/* add user code begin private includes */

/* add user code end private includes */

/* private typedef -----------------------------------------------------------*/
/* add user code begin private typedef */

/* add user code end private typedef */

/* private define ------------------------------------------------------------*/
/* add user code begin private define */

/* add user code end private define */

/* private macro -------------------------------------------------------------*/
/* add user code begin private macro */

/* add user code end private macro */

/* private variables ---------------------------------------------------------*/
/* add user code begin private variables */

/* add user code end private variables */

/* private function prototypes --------------------------------------------*/
/* add user code begin function prototypes */
void TXVolTemper(void);
void TXTemper(void);
void TXHumid(void);
/* add user code end function prototypes */

/* private user code ---------------------------------------------------------*/
/* add user code begin 0 */
uint16_t codeCtrl = 0xAB;
//Temp: 6E 30 2E 76 61 6C 3D 3? 3? ff ff ff
//Humid: 6E 31 2E 76 61 6C 3D 3? 3? ff ff ff
//�������� ����������� (����������): 6E 32 2E 76 61 6C 3D 3? 3? ff ff ff
//�������� ����������� (��������): 68 30 2E 76 61 6C 3D 3? 3? ff ff ff

uint16_t Temper = 21;
uint16_t TemperVar = 0;
uint16_t Humidity = 46;
uint16_t HumidityVar = 0;
uint16_t VolTemper = 24;

uint16_t dataTemp[] = { 0x6E, 0x30, 0x2E, 0x76, 0x61, 0x6C, 0x3D, 0x30, 0x30,
		0xFF };
uint16_t dataHum[] = { 0x6E, 0x31, 0x2E, 0x76, 0x61, 0x6C, 0x3D, 0x30, 0x30,
		0xFF };
uint16_t dataVolTemp[] = { 0x6E, 0x32, 0x2E, 0x76, 0x61, 0x6C, 0x3D, 0x30, 0x30,
		0xFF };
uint16_t dataSliderTemper[] = { 0x68, 0x30, 0x2E, 0x76, 0x61, 0x6C, 0x3D, 0x30,
		0x30, 0xFF };

/* add user code end 0 */

/**
 * @brief main function.
 * @param  none
 * @retval none
 */
int main(void) {
	/* add user code begin 1 */

	/* add user code end 1 */

	/* system clock config. */
	wk_system_clock_config();

	/* config periph clock. */
	wk_periph_clock_config();

	/* nvic config. */
	wk_nvic_config();

	/* init usart1 function. */
	wk_usart1_init();

	/* init usart2 function. */
	wk_usart2_init();

	/* init gpio function. */
	wk_gpio_config();

	/* add user code begin 2 */
	usart_interrupt_enable(USART1, USART_RDBF_INT, TRUE);
	usart_interrupt_enable(USART2, USART_RDBF_INT, TRUE);

	delay_init();

	delay_ms(400);

	gpio_bits_write(GPIOA, GPIO_PINS_0, FALSE);
	gpio_bits_write(GPIOA, GPIO_PINS_1, TRUE);
	gpio_bits_write(GPIOA, GPIO_PINS_5, TRUE);
	gpio_bits_write(GPIOA, GPIO_PINS_7, FALSE);
	delay_ms(50);

	gpio_bits_write(GPIOA, GPIO_PINS_0, TRUE);

	delay_ms(50);

	gpio_bits_write(GPIOA, GPIO_PINS_0, FALSE);

	/* add user code end 2 */

	while (1) {
		/* add user code begin 3 */

		if (Temper != TemperVar) {
			TemperVar = Temper;
			dataTemp[7] = 0x30;
			dataTemp[8] = 0x30;
			dataTemp[7] += (uint16_t) (Temper / 10);
			dataTemp[8] += (uint16_t) (Temper % 10);
		}

		if (Humidity != HumidityVar) {
			HumidityVar = Humidity;
			dataHum[7] = 0x30;
			dataHum[8] = 0x30;
			dataHum[7] += (uint16_t) (Humidity / 10);
			dataHum[8] += (uint16_t) (Humidity % 10);
		}

		TXTemper();
		TXHumid();

		delay_ms(5000);
		/* add user code end 3 */
	}

}

void USART1_IRQHandler(void) {
	while(usart_flag_get (USART2, USART_TDC_FLAG) == RESET){
		delay_ms(1000);
	}
	usart_flag_clear(USART1, USART_RDBF_INT);
	if ((usart_data_receive(USART1)) == 0x94) {
		TXTemper();
		TXHumid();
		TXVolTemper();
		delay_ms(5);
		usart_data_receive(USART1);
		delay_ms(5);
	}
	if ((usart_data_receive(USART1)) == 0x99) {
		gpio_bits_write(GPIOA, GPIO_PINS_0, TRUE);
		delay_ms(20);
		gpio_bits_write(GPIOA, GPIO_PINS_0, FALSE);
		VolTemper = usart_data_receive(USART1);
		usart_data_transmit(USART2, 0x99);
		usart_data_transmit(USART2, VolTemper);
	}
}

void USART2_IRQHandler(void) {
	usart_flag_clear(USART2, USART_RDBF_FLAG);
	if ((usart_data_receive(USART2)) == codeCtrl) {
		while ((usart_data_receive(USART2)) == codeCtrl) {
		}
		usart_flag_clear(USART2, USART_RDBF_FLAG);
		delay_ms(3);
		Temper = usart_data_receive(USART2);
		gpio_bits_write(GPIOA, GPIO_PINS_1, TRUE);
		delay_ms(100);
		gpio_bits_write(GPIOA, GPIO_PINS_1, FALSE);
		delay_ms(100);
		gpio_bits_write(GPIOA, GPIO_PINS_1, TRUE);
	}



}

void TXVolTemper() {
	gpio_bits_write(GPIOA, GPIO_PINS_0, TRUE);

	dataVolTemp[7] = 0x30;
	dataVolTemp[8] = 0x30;
	dataVolTemp[7] += (uint16_t) (VolTemper / 10);
	dataVolTemp[8] += (uint16_t) (VolTemper % 10);

	dataSliderTemper[7] = 0x30;
	dataSliderTemper[8] = 0x30;
	dataSliderTemper[7] += (uint16_t) (VolTemper / 10);
	dataSliderTemper[8] += (uint16_t) (VolTemper % 10);

	for (int i = 0; i < 9; i++) {
		usart_data_transmit(USART1, dataVolTemp[i]);
		delay_ms(3);
	}
	usart_data_transmit(USART1, dataVolTemp[9]);
	usart_data_transmit(USART1, dataVolTemp[9]);
	delay_ms(5);
	usart_data_transmit(USART1, dataVolTemp[9]);

	for (int i = 0; i < 9; i++) {
		usart_data_transmit(USART1, dataSliderTemper[i]);
		delay_ms(3);
	}
	usart_data_transmit(USART1, dataSliderTemper[9]);
	usart_data_transmit(USART1, dataSliderTemper[9]);
	delay_ms(5);
	usart_data_transmit(USART1, dataSliderTemper[9]);

	gpio_bits_write(GPIOA, GPIO_PINS_0, FALSE);
}

void TXTemper() {
	gpio_bits_write(GPIOA, GPIO_PINS_0, TRUE);

	for (int i = 0; i < 9; i++) {
		usart_data_transmit(USART1, dataTemp[i]);
		delay_ms(3);
	}
	usart_data_transmit(USART1, dataTemp[9]);
	usart_data_transmit(USART1, dataTemp[9]);
	delay_ms(5);
	usart_data_transmit(USART1, dataTemp[9]);

	gpio_bits_write(GPIOA, GPIO_PINS_0, FALSE);
}

void TXHumid() {
	gpio_bits_write(GPIOA, GPIO_PINS_0, TRUE);

	for (int i = 0; i < 9; i++) {
		usart_data_transmit(USART1, dataHum[i]);
		delay_ms(3);
	}
	usart_data_transmit(USART1, dataHum[9]);
	usart_data_transmit(USART1, dataHum[9]);
	delay_ms(5);
	usart_data_transmit(USART1, dataHum[9]);

	gpio_bits_write(GPIOA, GPIO_PINS_0, FALSE);
}

