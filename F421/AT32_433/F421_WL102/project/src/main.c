/* add user code begin Header */
/**
  **************************************************************************
  * @file     main.c
  * @brief    main program
  **************************************************************************
  *                       Copyright notice & Disclaimer
  *
  * The software Board Support Package (BSP) that is made available to
  * download from Artery official website is the copyrighted work of Artery.
  * Artery authorizes customers to use, copy, and distribute the BSP
  * software and its related documentation for the purpose of design and
  * development in conjunction with Artery microcontrollers. Use of the
  * software is governed by this copyright notice and the following disclaimer.
  *
  * THIS SOFTWARE IS PROVIDED ON "AS IS" BASIS WITHOUT WARRANTIES,
  * GUARANTEES OR REPRESENTATIONS OF ANY KIND. ARTERY EXPRESSLY DISCLAIMS,
  * TO THE FULLEST EXTENT PERMITTED BY LAW, ALL EXPRESS, IMPLIED OR
  * STATUTORY OR OTHER WARRANTIES, GUARANTEES OR REPRESENTATIONS,
  * INCLUDING BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.
  *
  **************************************************************************
  */
/* add user code end Header */

/* Includes ------------------------------------------------------------------*/
#include "at32f421_wk_config.h"

/* private includes ----------------------------------------------------------*/
/* add user code begin private includes */
#include "bmp180_for_stm32_hal.h"
/* add user code end private includes */

/* private typedef -----------------------------------------------------------*/
/* add user code begin private typedef */

/* add user code end private typedef */

/* private define ------------------------------------------------------------*/
/* add user code begin private define */

/* add user code end private define */

/* private macro -------------------------------------------------------------*/
/* add user code begin private macro */

/* add user code end private macro */

/* private variables ---------------------------------------------------------*/
/* add user code begin private variables */
i2c_handle_type hi2c1;
/* add user code end private variables */

/* private function prototypes --------------------------------------------*/
/* add user code begin function prototypes */

/* add user code end function prototypes */

/* private user code ---------------------------------------------------------*/
/* add user code begin 0 */
uint16_t Temper;
int32_t Pressure;
float Altitude;
/* add user code end 0 */

/**
  * @brief main function.
  * @param  none
  * @retval none
  */
int main(void)
{
  /* add user code begin 1 */

  /* add user code end 1 */

  /* system clock config. */
  wk_system_clock_config();

  /* config periph clock. */
  wk_periph_clock_config();

  /* nvic config. */
  wk_nvic_config();

  /* init dma1 channel1 */
  wk_dma1_channel1_init();
  /* config dma channel transfer parameter */
  /* user need to modify parameters memory_base_addr and buffer_size */
  wk_dma_channel_config(DMA1_CHANNEL1, 0, 0, 0);
  dma_channel_enable(DMA1_CHANNEL1, TRUE);

  /* init usart1 function. */
  wk_usart1_init();

  /* init usart2 function. */
  wk_usart2_init();

  /* init i2c1 function. */
  wk_i2c1_init();

  /* init gpio function. */
  wk_gpio_config();

  /* add user code begin 2 */
  delay_init();

  delay_ms(500);
  gpio_bits_write(GPIOB, GPIO_PINS_1, TRUE);

  hi2c1.i2cx = I2C1;

  delay_ms(10);

  BMP180_Init(&hi2c1);

  delay_ms(10);

  BMP180_SetOversampling(0);

  delay_ms(10);

  BMP180_UpdateCalibrationData();

  uint16_t data1 = 0xAA;
  uint16_t code = 0xBB;
  uint16_t data2 = 0;
  uint16_t data3 = 0;
  uint16_t data4 = 0;
  uint16_t data5 = 0;
  uint16_t Crc = 0;

//  uint16_t data1 = 0xAA;
//  uint16_t code = 0xBB;
//  uint16_t data2 = 0x96;
//  uint16_t data3 = 0x4D;
//  uint16_t data4 = 0xE6;
//  uint16_t data5 = 0x73;

  /* add user code end 2 */

  while(1)
  {
    /* add user code begin 3 */
	  gpio_bits_write(GPIOA, GPIO_PINS_15, TRUE);
	  delay_ms(500);
	  gpio_bits_write(GPIOB, GPIO_PINS_0, TRUE);

	  Temper = BMP180_GetTemperature();

	  delay_ms(1);

	  Pressure = BMP180_GetPressure();
	  Pressure = Pressure/100;

	  delay_ms(1);

	  data2 = Temper;
	  data3 = (uint16_t)(Pressure/100);
	  data4 = (uint16_t)(Pressure%100);
	  Crc = ((data2 + data3 + data4)/5);
	  data5 = Crc;


	  for (int i = 0; i < 25; i++){
		  usart_data_transmit (USART1, data1);
		  delay_ms(3);
	  }
	  //delay_ms(3);
	  usart_data_transmit (USART1, code);
	  delay_ms(3);
	  usart_data_transmit (USART1, data2);
	  delay_ms(3);
	  usart_data_transmit (USART1, data3);
	  delay_ms(3);
	  usart_data_transmit (USART1, data4);
	  delay_ms(3);
	  usart_data_transmit (USART1, data5);
	  gpio_bits_write(GPIOB, GPIO_PINS_0, FALSE);
	  gpio_bits_write(GPIOA, GPIO_PINS_15, FALSE);
	  delay_ms(5000);

    /* add user code end 3 */
  }
}
