/* add user code begin Header */
/**
  **************************************************************************
  * @file     main.c
  * @brief    main program
  **************************************************************************
  *                       Copyright notice & Disclaimer
  *
  * The software Board Support Package (BSP) that is made available to
  * download from Artery official website is the copyrighted work of Artery.
  * Artery authorizes customers to use, copy, and distribute the BSP
  * software and its related documentation for the purpose of design and
  * development in conjunction with Artery microcontrollers. Use of the
  * software is governed by this copyright notice and the following disclaimer.
  *
  * THIS SOFTWARE IS PROVIDED ON "AS IS" BASIS WITHOUT WARRANTIES,
  * GUARANTEES OR REPRESENTATIONS OF ANY KIND. ARTERY EXPRESSLY DISCLAIMS,
  * TO THE FULLEST EXTENT PERMITTED BY LAW, ALL EXPRESS, IMPLIED OR
  * STATUTORY OR OTHER WARRANTIES, GUARANTEES OR REPRESENTATIONS,
  * INCLUDING BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.
  *
  **************************************************************************
  */
/* add user code end Header */

/* Includes ------------------------------------------------------------------*/
#include "at32a403a_wk_config.h"

/* private includes ----------------------------------------------------------*/
/* add user code begin private includes */
#define DELAY                            250

uint16_t button_press = 0;
/* add user code end private includes */

/* private typedef -----------------------------------------------------------*/
/* add user code begin private typedef */

/* add user code end private typedef */

/* private define ------------------------------------------------------------*/
/* add user code begin private define */

/* add user code end private define */

/* private macro -------------------------------------------------------------*/
/* add user code begin private macro */

/* add user code end private macro */

/* private variables ---------------------------------------------------------*/
/* add user code begin private variables */

/* add user code end private variables */

/* private function prototypes --------------------------------------------*/
/* add user code begin function prototypes */

/* add user code end function prototypes */

/* private user code ---------------------------------------------------------*/
/* add user code begin 0 */
void button_isr(void)
{
  /* delay 5ms */
  delay_ms(5);

  /* clear interrupt pending bit */
  exint_flag_clear(EXINT_LINE_0);

  /* check input pin state */
  if(SET == gpio_input_data_bit_read(GPIOA, GPIO_PINS_0))
  {
	  button_press++;
	  printf("Button is pressed %u", button_press);
	  printf(" time\n\r");
  }
}

void at32_led1_off() {
	GPIOD->scr = GPIO_PINS_13;
}

void at32_led2_off() {
	GPIOD->scr = GPIO_PINS_14;
}

void at32_led3_off() {
	GPIOD->scr = GPIO_PINS_15;
}

void at32_led1_on() {
	GPIOD->clr = GPIO_PINS_13;
}

void at32_led2_on() {
	GPIOD->clr = GPIO_PINS_14;
}

void at32_led3_on() {
	GPIOD->clr = GPIO_PINS_15;
}
/* add user code end 0 */

/**
  * @brief main function.
  * @param  none
  * @retval none
  */
int main(void)
{
	/* add user code begin 1 */
		at32_board_init();
		at32_led1_off();
		at32_led2_off();
		at32_led3_off();

		uint16_t a = 0;
	  /* add user code end 1 */

	  /* add user code begin 2 */
		printf("AT32 started with USART1\r\n");
	  /* add user code end 2 */

	  while(1)
	  {
		  if ((button_press % 2) == 0) {
			  /* add user code begin 3 */
			  at32_led1_on();
		  	  delay_ms(DELAY);
		  	  at32_led2_on();
		  	  delay_ms(DELAY);
		  	  at32_led3_on();
		  	  delay_ms(DELAY);
		  	  at32_led1_off();
		  	  delay_ms(DELAY);
		  	  at32_led2_off();
		  	  delay_ms(DELAY);
		  	  at32_led3_off();
		  	  delay_ms(DELAY);
		  	  //printf("The full cycle was completed in %u", DELAY*6);
		  	  //printf (" ms \n\r");
		  } else {
			  at32_led3_on();
			  delay_ms(DELAY);
			  at32_led2_on();
			  delay_ms(DELAY);
			  at32_led1_on();
			  delay_ms(DELAY);
			  at32_led3_off();
			  delay_ms(DELAY);
			  at32_led2_off();
			  delay_ms(DELAY);
			  at32_led1_off();
			  delay_ms(DELAY);
		  }
	    /* add user code end 3 */
	  }
}
