/* add user code begin Header */
/**
  **************************************************************************
  * @file     main.c
  * @brief    main program
  **************************************************************************
  *                       Copyright notice & Disclaimer
  *
  * The software Board Support Package (BSP) that is made available to
  * download from Artery official website is the copyrighted work of Artery.
  * Artery authorizes customers to use, copy, and distribute the BSP
  * software and its related documentation for the purpose of design and
  * development in conjunction with Artery microcontrollers. Use of the
  * software is governed by this copyright notice and the following disclaimer.
  *
  * THIS SOFTWARE IS PROVIDED ON "AS IS" BASIS WITHOUT WARRANTIES,
  * GUARANTEES OR REPRESENTATIONS OF ANY KIND. ARTERY EXPRESSLY DISCLAIMS,
  * TO THE FULLEST EXTENT PERMITTED BY LAW, ALL EXPRESS, IMPLIED OR
  * STATUTORY OR OTHER WARRANTIES, GUARANTEES OR REPRESENTATIONS,
  * INCLUDING BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.
  *
  **************************************************************************
  */
/* add user code end Header */

/* Includes ------------------------------------------------------------------*/
#include "at32f403a_407_wk_config.h"

/* private includes ----------------------------------------------------------*/
/* add user code begin private includes */
//#include "BMP180.h"
#include "bmp180_for_stm32_hal.h"
/* add user code end private includes */

/* private typedef -----------------------------------------------------------*/
/* add user code begin private typedef */

/* add user code end private typedef */

/* private define ------------------------------------------------------------*/
/* add user code begin private define */

/* add user code end private define */

/* private macro -------------------------------------------------------------*/
/* add user code begin private macro */

/* add user code end private macro */

/* private variables ---------------------------------------------------------*/
/* add user code begin private variables */

int reg = 0;
i2c_handle_type hi2c1;

/* add user code end private variables */

/* private function prototypes --------------------------------------------*/
/* add user code begin function prototypes */

/* add user code end function prototypes */

/* private user code ---------------------------------------------------------*/
/* add user code begin 0 */
int32_t Temper;
float Pressure;
float Altitude;
/* add user code end 0 */

/**
  * @brief main function.
  * @param  none
  * @retval none
  */
int main(void)
{
  /* add user code begin 1 */

  /* add user code end 1 */

  /* system clock config. */
  wk_system_clock_config();

  /* config periph clock. */
  wk_periph_clock_config();

  /* init debug function. */
  wk_debug_config();

  /* nvic config. */
  wk_nvic_config();

  /* init dma1 channel1 */
  //wk_dma1_channel1_init();
  /* config dma channel transfer parameter */
  /* user need to modify parameters memory_base_addr and buffer_size */
  //wk_dma_channel_config(DMA1_CHANNEL1, 0, 0, 0);
  //dma_channel_enable(DMA1_CHANNEL1, TRUE);

  /* init usart1 function. */
  wk_usart1_init();

  /* init i2c1 function. */
  wk_i2c1_init();

  /* add user code begin 2 */
  delay_init();

  delay_ms(10);

  hi2c1.i2cx = I2C1;

  BMP180_Init(&hi2c1);

  BMP180_SetOversampling(1);

  delay_ms(10);

  BMP180_UpdateCalibrationData();

  delay_ms(1);
  //BMP180_Start();
  //i2c_start_generate(I2C1);
   //delay_us(500);
   //if (I2C1->sts1_bit.startf == TRUE){
 	  //delay_ms(1);
 	  //reg = I2C1->sts1;
 	  //delay_us(50);
 	  //i2c_7bit_address_send(I2C1, 0xEE, I2C_DIRECTION_RECEIVE);
 	  //i2c_data_send(I2C1, 0xEE);
 	  //delay_us(500);
 	  //BMP180_Start();
 	  //Callib_Data[0] = i2c_data_receive(I2C1);
 	  //delay_us(500);
 	  //i2c_stop_generate(I2C1);
   //}
  /* add user code end 2 */

  while(1)
  {
    /* add user code begin 3 */
	  Temper = BMP180_GetTemperature();
	  Pressure = BMP180_GetPressure();
	  usart_data_transmit (USART1, Temper);
	  //Temper = BMP180_GetTemp();
	  //Pressure = BMP180_GetPress(0);
	  //USART1->dt = Temper;
	  //Altitude = BMP180_GetAlt(0);
	  delay_ms(1000);
    /* add user code end 3 */
  }
}
