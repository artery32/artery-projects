/* add user code begin Header */
/**
 **************************************************************************
 * @file     main.c
 * @brief    main program
 **************************************************************************
 *                       Copyright notice & Disclaimer
 *
 * The software Board Support Package (BSP) that is made available to
 * download from Artery official website is the copyrighted work of Artery.
 * Artery authorizes customers to use, copy, and distribute the BSP
 * software and its related documentation for the purpose of design and
 * development in conjunction with Artery microcontrollers. Use of the
 * software is governed by this copyright notice and the following disclaimer.
 *
 * THIS SOFTWARE IS PROVIDED ON "AS IS" BASIS WITHOUT WARRANTIES,
 * GUARANTEES OR REPRESENTATIONS OF ANY KIND. ARTERY EXPRESSLY DISCLAIMS,
 * TO THE FULLEST EXTENT PERMITTED BY LAW, ALL EXPRESS, IMPLIED OR
 * STATUTORY OR OTHER WARRANTIES, GUARANTEES OR REPRESENTATIONS,
 * INCLUDING BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.
 *
 **************************************************************************
 */
/* add user code end Header */

/* Includes ------------------------------------------------------------------*/
#include "at32f403a_407_wk_config.h"
#include "wk_system.h"

/* private includes ----------------------------------------------------------*/
/* add user code begin private includes */

/* add user code end private includes */

/* private typedef -----------------------------------------------------------*/
/* add user code begin private typedef */

/* add user code end private typedef */

/* private define ------------------------------------------------------------*/
/* add user code begin private define */
#define GSM_SLEEP TRUE
#define GSM_WAKEUP FALSE
/* add user code end private define */

/* private macro -------------------------------------------------------------*/
/* add user code begin private macro */
calendar_type alarm_struct;
calendar_type time_struct;
/* add user code end private macro */

/* private variables ---------------------------------------------------------*/
/* add user code begin private variables */
volatile uint32_t timer1 = 0;
uint16_t ADC1_Bat_value = 0;
uint8_t Sleep_Counter = 0;
uint8_t Connect_Count = 0;
uint8_t i = 0;
uint8_t connect_mobile = 0; //connect to network: 0 (false) or 1 (true)
uint8_t zipcall_count = 0;
uint8_t send_count = 0;
char ADC1_Bat[32];
char Answer_Recieve[64];
extern char AT_command[6];
extern const char Manufacturer_module[10];
extern const char Model_module[10];
extern const char Revision_module[10];
extern const char CFUN0[16];
extern const char CFUN1[16]; // @suppress("Unused variable declaration in file scope")
extern const char CFUN4[16]; // @suppress("Unused variable declaration in file scope")
extern const char ZPAS[16];
extern const char ZIPCFG[24];
extern const char ZIPCALL1[16];
extern const char ZIPCALL0[16];
extern char ZHTTPURL[96];
extern const char ZHTTPGET[14];

//char URL[96] = "https://api.thingspeak.com/update?api_key=Y2DIGCSQALVSOWE4&field1=\0";

char const NameGSM_module_toUART1[16] = "Name module: ";
char const Send_msg_hello[12] = "Hello!\r\n";
char const Send_msg_GSM_Init[32] = "Initialization GSM-module...\r\n";
char Send_msg_ADC_Bat[48] = "Value ADC on battery line: ";
char const Send_msg_sleep[24] = "MCU is sleeping...\r\n";
char const Send_msg_wakeup[24] = "MCU is waking up...\r\n";
char const Send_msg_reset[24] = "MCU is restarting...\r\n\0";
char const Send_msg_all_working[42] = "MCU and GSM-module is working!\r\n\0";
char const Msg_Enter_to_DeepSleep[32] = "MCU entering to deep sleep...\r\n\0";
char const Msg_Connect_toNetwork[32] = "Connecting to network...\r\n\0";
char const Msg_Connect_Success[32] = "Successful connection!\r\n\0";
char const Msg_Connect_Error[32] = "Connection failed!\r\n\0";
char const Msg_Success_Send[32] = "Sending successful!\r\n\0";


/* add user code end private variables */

/* private function prototypes --------------------------------------------*/
/* add user code begin function prototypes */

/* add user code end function prototypes */

/* private user code ---------------------------------------------------------*/
/* add user code begin 0 */

/* add user code end 0 */

/**
 * @brief main function.
 * @param  none
 * @retval none
 */
int main(void) {
	/* add user code begin 1 */

	/* add user code end 1 */

	/* system clock config. */
	wk_system_clock_config();

	/* config periph clock. */
	wk_periph_clock_config();

	/* init debug function. */
	wk_debug_config();

	/* init pwc function. */
	wk_pwc_init();

	/* nvic config. */
	wk_nvic_config();

	/* timebase config. */
	wk_timebase_init();

	/* init dma1 channel1 */
	wk_dma1_channel1_init();
	/* config dma channel transfer parameter */
	/* user need to modify define values DMAx_CHANNELy_XXX_BASE_ADDR and DMAx_CHANNELy_BUFFER_SIZE in at32xxx_wk_config.h */
//  wk_dma_channel_config(DMA1_CHANNEL1,
//                        (uint32_t)&USART2->dt,
//                        DMA1_CHANNEL1_MEMORY_BASE_ADDR,
//                        DMA1_CHANNEL1_BUFFER_SIZE);
//  dma_channel_enable(DMA1_CHANNEL1, TRUE);
	/* init dma1 channel2 */
	wk_dma1_channel2_init();
	/* config dma channel transfer parameter */
	/* user need to modify define values DMAx_CHANNELy_XXX_BASE_ADDR and DMAx_CHANNELy_BUFFER_SIZE in at32xxx_wk_config.h */
//  wk_dma_channel_config(DMA1_CHANNEL2,
//                        (uint32_t)&USART2->dt,
//                        DMA1_CHANNEL2_MEMORY_BASE_ADDR,
//                        DMA1_CHANNEL2_BUFFER_SIZE);
//  dma_channel_enable(DMA1_CHANNEL2, TRUE);
	/* init dma1 channel3 */
	wk_dma1_channel3_init();
	/* config dma channel transfer parameter */
	/* user need to modify define values DMAx_CHANNELy_XXX_BASE_ADDR and DMAx_CHANNELy_BUFFER_SIZE in at32xxx_wk_config.h */
//  wk_dma_channel_config(DMA1_CHANNEL3,
//                        (uint32_t)&USART1->dt,
//                        DMA1_CHANNEL3_MEMORY_BASE_ADDR,
//                        DMA1_CHANNEL3_BUFFER_SIZE);
//  dma_channel_enable(DMA1_CHANNEL3, TRUE);
	/* init dma1 channel4 */
	wk_dma1_channel4_init();
	/* config dma channel transfer parameter */
	/* user need to modify define values DMAx_CHANNELy_XXX_BASE_ADDR and DMAx_CHANNELy_BUFFER_SIZE in at32xxx_wk_config.h */
//  wk_dma_channel_config(DMA1_CHANNEL4,
//                        (uint32_t)&USART1->dt,
//                        DMA1_CHANNEL4_MEMORY_BASE_ADDR,
//                        DMA1_CHANNEL4_BUFFER_SIZE);
//  dma_channel_enable(DMA1_CHANNEL4, TRUE);
	/* init usart1 function. */
	wk_usart1_init();

	/* init usart2 function. */
	wk_usart2_init();

	/* init uart4 function. */
	//wk_uart4_init();
	/* init uart5 function. */
	//wk_uart5_init();
	/* init spi1 function. */
	//wk_spi1_init();
	/* init spi2 function. */
	//wk_spi2_init();
	/* init i2c2 function. */
	//wk_i2c2_init();
	/* init adc1 function. */
	wk_adc1_init();

	/* init rtc function. */
	wk_rtc_init();

	/* init exint function. */
	wk_exint_config();

	/* init wdt function. */
	wk_wdt_init();

	/* init gpio function. */
	wk_gpio_config();

	/* add user code begin 2 */
	wdt_enable();

	gpio_bits_write(LoRa900T22D_M0_GPIO_PORT, LoRa900T22D_M0_PIN, FALSE);
	gpio_bits_write(LoRa900T22D_M1_GPIO_PORT, LoRa900T22D_M1_PIN, FALSE);
	gpio_bits_write(Sleep_GSM_GPIO_PORT, Sleep_GSM_PIN, TRUE);

	usart_interrupt_enable(USART1, USART_RDBF_INT, TRUE);

	adc_ordinary_software_trigger_enable(ADC1, TRUE);

	wk_delay_ms(1000);

	wdt_counter_reload();

	ADC1_Bat_value = adc_ordinary_conversion_data_get(ADC1);

	sprintf(ADC1_Bat, "%u", ADC1_Bat_value);
	strcat(ADC1_Bat, "\r\n");
	strcat(Send_msg_ADC_Bat, ADC1_Bat);

	strcat(ZHTTPURL, ADC1_Bat);

	gpio_bits_write(LED5_GPIO_PORT, LED5_PIN, TRUE);
	gpio_bits_write(LED6_GPIO_PORT, LED6_PIN, TRUE);

	wdt_counter_reload();

	//UART1_Transmit(Send_msg_hello);

	wk_dma_channel_config(DMA1_CHANNEL3, (uint32_t) &USART1->dt,
			(uint32_t) &Send_msg_hello[0], strlen(Send_msg_hello));
	dma_channel_enable(DMA1_CHANNEL3, TRUE);

	wk_delay_ms(10);
	gpio_bits_write(LED6_GPIO_PORT, LED6_PIN, FALSE);
	dma_channel_enable(DMA1_CHANNEL3, FALSE);

	gpio_bits_write(LED6_GPIO_PORT, LED6_PIN, TRUE);
	wk_delay_ms(50);
	gpio_bits_write(LED6_GPIO_PORT, LED6_PIN, FALSE);

	wk_dma_channel_config(DMA1_CHANNEL3, (uint32_t) &USART1->dt,
			(uint32_t) &Send_msg_ADC_Bat[0], strlen(Send_msg_ADC_Bat));
	dma_channel_enable(DMA1_CHANNEL3, TRUE);
	wk_delay_ms(50);
	dma_channel_enable(DMA1_CHANNEL3, FALSE);

	gpio_bits_write(LED6_GPIO_PORT, LED6_PIN, TRUE);
	wk_delay_ms(50);
	gpio_bits_write(LED6_GPIO_PORT, LED6_PIN, FALSE);

	//UART1_Transmit(Send_msg_ADC_Bat);

	//wk_delay_ms(50);

	//UART1_Transmit(Send_msg_GSM_Init);

	wk_dma_channel_config(DMA1_CHANNEL3, (uint32_t) &USART1->dt,
			(uint32_t) &Send_msg_GSM_Init[0], strlen(Send_msg_GSM_Init));
	dma_channel_enable(DMA1_CHANNEL3, TRUE);
	wk_delay_ms(50);
	dma_channel_enable(DMA1_CHANNEL3, FALSE);

	gpio_bits_write(LED6_GPIO_PORT, LED6_PIN, TRUE);
	wk_delay_ms(50);
	gpio_bits_write(LED6_GPIO_PORT, LED6_PIN, FALSE);

	gpio_bits_write(Reset_GSM_GPIO_PORT, Reset_GSM_PIN, TRUE);
	wk_delay_ms(50);
	gpio_bits_write(Reset_GSM_GPIO_PORT, Reset_GSM_PIN, FALSE);

	gpio_bits_write(LED6_GPIO_PORT, LED6_PIN, TRUE);
	wk_delay_ms(50);
	gpio_bits_write(LED6_GPIO_PORT, LED6_PIN, FALSE);

	wdt_counter_reload();

	while (i < 30) {
		i++;
		gpio_bits_write(LED6_GPIO_PORT, LED6_PIN, TRUE);
		wk_delay_ms(20);
		gpio_bits_write(LED6_GPIO_PORT, LED6_PIN, FALSE);
		wk_delay_ms(480);
		wdt_counter_reload();
	}

	i = 0;

	//AT_Transmit(AT_command);

	wk_dma_channel_config(DMA1_CHANNEL1, (uint32_t) &USART2->dt,
			(uint32_t) &AT_command[0], 4);
	dma_channel_enable(DMA1_CHANNEL1, TRUE);

	wk_delay_ms(1200);

	gpio_bits_write(LED6_GPIO_PORT, LED6_PIN, TRUE);
	wk_delay_ms(50);
	gpio_bits_write(LED6_GPIO_PORT, LED6_PIN, FALSE);

	dma_channel_enable(DMA1_CHANNEL1, FALSE);

	usart_interrupt_enable(USART2, USART_RDBF_INT, TRUE);

	wk_delay_ms(100);

	gpio_bits_write(LED6_GPIO_PORT, LED6_PIN, TRUE);
	wk_delay_ms(50);
	gpio_bits_write(LED6_GPIO_PORT, LED6_PIN, FALSE);

	wk_dma_channel_config(DMA1_CHANNEL3, (uint32_t) &USART1->dt,
			(uint32_t) &NameGSM_module_toUART1[0],
			strlen(NameGSM_module_toUART1));
	dma_channel_enable(DMA1_CHANNEL3, TRUE);

	wdt_counter_reload();

	//UART1_Transmit(NameGSM_module_toUART1);

	wk_delay_ms(50);
	dma_channel_enable(DMA1_CHANNEL3, FALSE);

	gpio_bits_write(LED6_GPIO_PORT, LED6_PIN, TRUE);
	wk_delay_ms(50);
	gpio_bits_write(LED6_GPIO_PORT, LED6_PIN, FALSE);

	//AT_Transmit(Manufacturer_module);

	wk_dma_channel_config(DMA1_CHANNEL1, (uint32_t) &USART2->dt,
			(uint32_t) &Manufacturer_module[0], strlen(Manufacturer_module));
	dma_channel_enable(DMA1_CHANNEL1, TRUE);
	wk_delay_ms(50);
	dma_channel_enable(DMA1_CHANNEL1, FALSE);

	gpio_bits_write(LED6_GPIO_PORT, LED6_PIN, TRUE);
	wk_delay_ms(50);
	gpio_bits_write(LED6_GPIO_PORT, LED6_PIN, FALSE);

	//AT_Transmit(Model_module);

	wk_dma_channel_config(DMA1_CHANNEL1, (uint32_t) &USART2->dt,
			(uint32_t) &Model_module[0], strlen(Model_module));
	dma_channel_enable(DMA1_CHANNEL1, TRUE);
	wk_delay_ms(50);
	dma_channel_enable(DMA1_CHANNEL1, FALSE);

	gpio_bits_write(LED6_GPIO_PORT, LED6_PIN, TRUE);
	wk_delay_ms(50);
	gpio_bits_write(LED6_GPIO_PORT, LED6_PIN, FALSE);

	//AT_Transmit(Revision_module);

	wk_dma_channel_config(DMA1_CHANNEL1, (uint32_t) &USART2->dt,
			(uint32_t) &Revision_module[0], strlen(Revision_module));
	dma_channel_enable(DMA1_CHANNEL1, TRUE);
	wk_delay_ms(50);
	dma_channel_enable(DMA1_CHANNEL1, FALSE);

	gpio_bits_write(LED6_GPIO_PORT, LED6_PIN, TRUE);
	wk_delay_ms(50);
	gpio_bits_write(LED6_GPIO_PORT, LED6_PIN, FALSE);

	usart_interrupt_enable(USART2, USART_RDBF_INT, FALSE);

	wdt_counter_reload();

	wk_dma_channel_config(DMA1_CHANNEL3, (uint32_t) &USART1->dt,
			(uint32_t) &Msg_Connect_toNetwork[0],
			strlen(Msg_Connect_toNetwork));
	dma_channel_enable(DMA1_CHANNEL3, TRUE);
	wk_delay_ms(50);
	dma_channel_enable(DMA1_CHANNEL3, FALSE);


	while ((Connect_Count < 100) && (strstr(Answer_Recieve, "LTE") == NULL) && (strstr(Answer_Recieve, "GPRS") == NULL) && (strstr(Answer_Recieve, "EDGE") == NULL)) {

		Connect_Count++;

		wk_dma_channel_config(DMA1_CHANNEL1, (uint32_t) &USART2->dt,
				(uint32_t) &ZPAS[0], strlen(ZPAS));
		wk_dma_channel_config(DMA1_CHANNEL2, (uint32_t) &USART2->dt,
				(uint32_t) &Answer_Recieve[0], 16);
		dma_channel_enable(DMA1_CHANNEL2, TRUE);
		dma_channel_enable(DMA1_CHANNEL1, TRUE);
		wk_delay_ms(8);
		dma_channel_enable(DMA1_CHANNEL1, FALSE);
		wk_delay_ms(50);
		dma_channel_enable(DMA1_CHANNEL2, FALSE);

		wk_dma_channel_config(DMA1_CHANNEL3, (uint32_t) &USART1->dt,
				(uint32_t) &Answer_Recieve[0],
				strlen(Answer_Recieve));
		dma_channel_enable(DMA1_CHANNEL3, TRUE);
		wk_delay_ms(50);
		dma_channel_enable(DMA1_CHANNEL3, FALSE);

		while (i<6) {
			i++;
			gpio_bits_write(LED5_GPIO_PORT, LED5_PIN, FALSE);
			wk_delay_ms(50);
			gpio_bits_write(LED6_GPIO_PORT, LED6_PIN, FALSE);
			wk_delay_ms(50);
			gpio_bits_write(LED5_GPIO_PORT, LED5_PIN, TRUE);
			wk_delay_ms(50);
			gpio_bits_write(LED6_GPIO_PORT, LED6_PIN, TRUE);
			wk_delay_ms(50);
		}
		i = 0;
		wdt_counter_reload();
	}
	gpio_bits_write(LED6_GPIO_PORT, LED6_PIN, FALSE);

	if ((strstr(Answer_Recieve, "LTE") != NULL) || (strstr(Answer_Recieve, "GPRS") != NULL) || (strstr(Answer_Recieve, "EDGE") != NULL)) {
		wk_dma_channel_config(DMA1_CHANNEL3, (uint32_t) &USART1->dt,
			(uint32_t) &Msg_Connect_Success[0],
			strlen(Msg_Connect_Success));
		dma_channel_enable(DMA1_CHANNEL3, TRUE);
		wk_delay_ms(50);
		dma_channel_enable(DMA1_CHANNEL3, FALSE);
		wk_delay_ms(100);
		connect_mobile = 1;
	} else {
		wk_dma_channel_config(DMA1_CHANNEL3, (uint32_t) &USART1->dt,
				(uint32_t) &Msg_Connect_Error[0],
				strlen(Msg_Connect_Error));
		dma_channel_enable(DMA1_CHANNEL3, TRUE);
		wk_delay_ms(50);
		dma_channel_enable(DMA1_CHANNEL3, FALSE);
	}

	UART_clear_buffer(USART2);

	//usart_interrupt_enable(USART2, USART_RDBF_INT, TRUE);

	wdt_counter_reload();

	if (connect_mobile != 0) {
		Tranciever_onNetwork();
	}

	Enter_toSleep();

	//usart_interrupt_enable(USART2, USART_RDBF_INT, TRUE);

	/* add user code end 2 */

	while (1) {
		/* add user code begin 3 */

		Sleep_Mode_pulling();
		wdt_counter_reload();

		/* add user code end 3 */
	}
}

/* add user code begin 4 */

void Tranciever_onNetwork(void){

	wk_dma_channel_config(DMA1_CHANNEL1, (uint32_t) &USART2->dt,
			(uint32_t) &ZIPCFG[0], strlen(ZIPCFG));
	dma_channel_enable(DMA1_CHANNEL1, TRUE);
	wk_delay_ms(50);
	dma_channel_enable(DMA1_CHANNEL1, FALSE);

	wk_delay_ms(200);

	usart_interrupt_enable(USART2, USART_RDBF_INT, FALSE);

	gpio_bits_write(LED6_GPIO_PORT, LED6_PIN, TRUE);
	wk_delay_ms(200);
	gpio_bits_write(LED6_GPIO_PORT, LED6_PIN, FALSE);

//	wk_dma_channel_config(DMA1_CHANNEL1, (uint32_t) &USART2->dt,
//			(uint32_t) &ZIPCALL1[0], strlen(ZIPCALL1));
//	wk_dma_channel_config(DMA1_CHANNEL2, (uint32_t) &USART2->dt,
//			(uint32_t) &Answer_Recieve[0], 40);
//	dma_channel_enable(DMA1_CHANNEL2, TRUE);
//	dma_channel_enable(DMA1_CHANNEL1, TRUE);
//	wk_delay_ms(20);
//	dma_channel_enable(DMA1_CHANNEL1, FALSE);
//	wk_delay_ms(500);
//	wk_delay_ms(500);
//	wdt_counter_reload();
//	wk_delay_ms(500);
//	wk_delay_ms(500);
//	dma_channel_enable(DMA1_CHANNEL2, FALSE);
//	wk_delay_ms(30);
//	gpio_bits_write(LED6_GPIO_PORT, LED6_PIN, TRUE);
//	wk_delay_ms(200);
//	gpio_bits_write(LED6_GPIO_PORT, LED6_PIN, FALSE);
//
//	wdt_counter_reload();
//
//	wk_dma_channel_config(DMA1_CHANNEL3, (uint32_t) &USART1->dt,
//			(uint32_t) &Answer_Recieve[0],
//			strlen(Answer_Recieve));
//	dma_channel_enable(DMA1_CHANNEL3, TRUE);
//	wk_delay_ms(50);
//	dma_channel_enable(DMA1_CHANNEL3, FALSE);
//	wk_delay_ms(1000);

	while ((zipcall_count < 40) && (strstr(Answer_Recieve, "+ZIPCALL: 1") == NULL)){
		zipcall_count++;
		wk_dma_channel_config(DMA1_CHANNEL1, (uint32_t) &USART2->dt,
				(uint32_t) &ZIPCALL1[0], strlen(ZIPCALL1));
		wk_dma_channel_config(DMA1_CHANNEL2, (uint32_t) &USART2->dt,
				(uint32_t) &Answer_Recieve[0], 40);
		dma_channel_enable(DMA1_CHANNEL2, TRUE);
		dma_channel_enable(DMA1_CHANNEL1, TRUE);
		wk_delay_ms(20);
		dma_channel_enable(DMA1_CHANNEL1, FALSE);
		gpio_bits_write(LED6_GPIO_PORT, LED6_PIN, TRUE);
		wk_delay_ms(500);
		gpio_bits_write(LED6_GPIO_PORT, LED6_PIN, FALSE);
		wk_delay_ms(500);
		gpio_bits_write(LED6_GPIO_PORT, LED6_PIN, TRUE);
		wk_delay_ms(500);
		gpio_bits_write(LED6_GPIO_PORT, LED6_PIN, FALSE);
		wk_delay_ms(500);
		gpio_bits_write(LED6_GPIO_PORT, LED6_PIN, TRUE);
		dma_channel_enable(DMA1_CHANNEL2, FALSE);
		wk_delay_ms(30);

		wk_dma_channel_config(DMA1_CHANNEL3, (uint32_t) &USART1->dt,
					(uint32_t) &Answer_Recieve[0],
					strlen(Answer_Recieve));
		dma_channel_enable(DMA1_CHANNEL3, TRUE);
		wk_delay_ms(100);
		dma_channel_enable(DMA1_CHANNEL3, FALSE);
		gpio_bits_write(LED6_GPIO_PORT, LED6_PIN, FALSE);
		wdt_counter_reload();
	}

	if (strstr(Answer_Recieve, "+ZIPCALL: 1") != NULL) {
		wdt_counter_reload();
		wk_dma_channel_config(DMA1_CHANNEL3, (uint32_t) &USART1->dt,
				(uint32_t) &ZHTTPURL[0], strlen(ZHTTPURL));
		dma_channel_enable(DMA1_CHANNEL3, TRUE);
		wk_delay_ms(200);
		dma_channel_enable(DMA1_CHANNEL3, FALSE);

		wk_delay_ms(10);

		wk_dma_channel_config(DMA1_CHANNEL1, (uint32_t) &USART2->dt,
				(uint32_t) &ZHTTPURL[0], strlen(ZHTTPURL));
		dma_channel_enable(DMA1_CHANNEL1, TRUE);
		wk_delay_ms(200);
		dma_channel_enable(DMA1_CHANNEL1, FALSE);

		wk_delay_ms(1000);
		wdt_counter_reload();

		wk_dma_channel_config(DMA1_CHANNEL1, (uint32_t) &USART2->dt,
				(uint32_t) &ZHTTPGET[0], strlen(ZHTTPGET));
		wk_dma_channel_config(DMA1_CHANNEL2, (uint32_t) &USART2->dt,
				(uint32_t) &Answer_Recieve[0], 24);
		dma_channel_enable(DMA1_CHANNEL2, TRUE);
		dma_channel_enable(DMA1_CHANNEL1, TRUE);
		wk_delay_ms(50);
		dma_channel_enable(DMA1_CHANNEL1, FALSE);

		while ((send_count < 91) && (strstr(Answer_Recieve, "HTTP/1.1 200 OK") == NULL)) {
			send_count++;
			gpio_bits_write(LED6_GPIO_PORT, LED6_PIN, TRUE);
			wk_delay_ms(100);
			gpio_bits_write(LED6_GPIO_PORT, LED6_PIN, FALSE);
			wk_delay_ms(100);
			gpio_bits_write(LED6_GPIO_PORT, LED6_PIN, TRUE);
			wk_delay_ms(100);
			gpio_bits_write(LED6_GPIO_PORT, LED6_PIN, FALSE);
			wk_delay_ms(200);
			wdt_counter_reload();
		}

		if (strstr(Answer_Recieve, "HTTP/1.1 200 OK") != NULL) {
			dma_channel_enable(DMA1_CHANNEL2, FALSE);
			wk_dma_channel_config(DMA1_CHANNEL3, (uint32_t) &USART1->dt,
					(uint32_t) &Msg_Success_Send[0],
					strlen(Msg_Success_Send));
			dma_channel_enable(DMA1_CHANNEL3, TRUE);
			wk_delay_ms(100);
			dma_channel_enable(DMA1_CHANNEL3, FALSE);
			wdt_counter_reload();
		} else {
			wdt_counter_reload();
			wk_dma_channel_config(DMA1_CHANNEL1, (uint32_t) &USART2->dt,
					(uint32_t) &ZHTTPGET[0], strlen(ZHTTPGET));
			dma_channel_enable(DMA1_CHANNEL1, TRUE);
			wk_delay_ms(50);
			dma_channel_enable(DMA1_CHANNEL1, FALSE);
			send_count = 0;
			while ((send_count < 41) && (strstr(Answer_Recieve, "HTTP/1.1 200 OK") == NULL)) {
				send_count++;
				gpio_bits_write(LED6_GPIO_PORT, LED6_PIN, TRUE);
				wk_delay_ms(500);
				gpio_bits_write(LED6_GPIO_PORT, LED6_PIN, FALSE);
				wdt_counter_reload();
			}
		}
	}


	wk_dma_channel_config(DMA1_CHANNEL1, (uint32_t) &USART2->dt,
			(uint32_t) &ZIPCALL0[0], strlen(ZIPCALL0));
	dma_channel_enable(DMA1_CHANNEL1, TRUE);
	wk_delay_ms(50);
	dma_channel_enable(DMA1_CHANNEL1, FALSE);

	wdt_counter_reload();

}

void UART_clear_buffer(usart_type* usart_x){
	while (usart_flag_get(usart_x, USART_RDBF_FLAG) != RESET) {
		UNUSED(usart_x->dt);
	}
}

void Sleep_Mode_pulling(void){
	if (Sleep_Counter < 38) {
		dma_channel_enable(DMA1_CHANNEL3, FALSE);

		wdt_counter_reload();

		gpio_bits_write(LED6_GPIO_PORT, LED6_PIN, TRUE);
		wk_delay_ms(30);
		gpio_bits_write(LED6_GPIO_PORT, LED6_PIN, FALSE);

		Sleep_Counter++;

		wdt_counter_reload();

		pwc_voltage_regulate_set(PWC_REGULATOR_LOW_POWER);
		pwc_deep_sleep_mode_enter(PWC_DEEP_SLEEP_ENTER_WFI);
		} else {
		wk_dma_channel_config(DMA1_CHANNEL3, (uint32_t) &USART1->dt,
				(uint32_t) &Send_msg_reset[0], strlen(Send_msg_reset));
		dma_channel_enable(DMA1_CHANNEL3, TRUE);
		wk_delay_ms(50);
		dma_channel_enable(DMA1_CHANNEL3, FALSE);
		nvic_system_reset();
	}
}

void Enter_toSleep(void){
	RTC_Time_Alarm_Reload();
	rtc_interrupt_enable(RTC_TA_INT, TRUE);

	wdt_counter_reload();

	dma_channel_enable(DMA1_CHANNEL3, FALSE);

	wk_dma_channel_config(DMA1_CHANNEL3, (uint32_t) &USART1->dt,
			(uint32_t) &Msg_Enter_to_DeepSleep[0],
			strlen(Msg_Enter_to_DeepSleep));
	dma_channel_enable(DMA1_CHANNEL3, TRUE);
	wk_delay_ms(50);
	dma_channel_enable(DMA1_CHANNEL3, FALSE);

	wk_dma_channel_config(DMA1_CHANNEL1, (uint32_t) &USART2->dt,
			(uint32_t) &CFUN0[0], strlen(CFUN0));
	dma_channel_enable(DMA1_CHANNEL1, TRUE);
	wk_delay_ms(50);
	dma_channel_enable(DMA1_CHANNEL1, FALSE);
	wk_delay_ms(50);

	gpio_bits_write(LoRa900T22D_M0_GPIO_PORT, LoRa900T22D_M0_PIN, TRUE);
	gpio_bits_write(LoRa900T22D_M1_GPIO_PORT, LoRa900T22D_M1_PIN, TRUE);
	gpio_bits_write(JDY41_Set_GPIO_PORT, JDY41_Set_PIN, TRUE);

	GSM_Sleep();

	wk_delay_ms(1);

	adc_enable(ADC1, FALSE);

	gpio_bits_write(LED5_GPIO_PORT, LED5_PIN, FALSE);
	gpio_bits_write(LED6_GPIO_PORT, LED6_PIN, FALSE);
}

void GSM_Sleep(void) {
	gpio_bits_write(Sleep_GSM_GPIO_PORT, Sleep_GSM_PIN, FALSE);
	wk_delay_ms(500);
	gpio_bits_write(Sleep_GSM_GPIO_PORT, Sleep_GSM_PIN, TRUE);
	wk_delay_ms(500);
	gpio_bits_write(Sleep_GSM_GPIO_PORT, Sleep_GSM_PIN, FALSE);
	wk_delay_ms(1);
}

void GSM_WakeUp(void){
	gpio_bits_write(Sleep_GSM_GPIO_PORT, Sleep_GSM_PIN, TRUE);
	wk_delay_ms(10);
	AT_Transmit(AT_command);
	wk_delay_ms(1);
	AT_Transmit(AT_command);
	wk_delay_ms(1);
}

void RTC_Time_Alarm_Reload(void) {
	time_struct.year = 2025;
	time_struct.month = 1;
	time_struct.date = 1;
	time_struct.hour = 0;
	time_struct.min = 0;
	time_struct.sec = 0;
	rtc_time_set(&time_struct);

	alarm_struct.year = 2025;
	alarm_struct.month = 1;
	alarm_struct.date = 1;
	alarm_struct.hour = 0;
	alarm_struct.min = 0;
	alarm_struct.sec = 24;
	rtc_alarm_clock_set(&alarm_struct);
}

void AT_Transmit(char *command) {
	uint8_t i = 0;
	//command+=0;
	while (command[i]) {
		Usart2_Send_symbol(command[i]);
		wk_delay_ms(1);
		i++;
	}
	wk_delay_ms(50);

}

void UART1_Transmit(char *text) {
	uint8_t i = 0;
	while (text[i]) {
		Usart1_Send_symbol(text[i]);
		wk_delay_ms(2);
		i++;
	}
	wk_delay_ms(50);
}

void Answer_TX_Uart1(char *answer) {
	i = 0;
	//command+=0;
	while (answer[i]) {
		Usart1_Send_symbol(answer[i]);
		wk_delay_ms(1);
		i++;
	}
	Answer_Recieve[0] = '\0';
	i = 0;
}

void Usart2_Send_symbol(uint8_t data) {
	USART2->dt = data;
}

void Usart1_Send_symbol(uint8_t data) {
	USART1->dt = data;
}

uint8_t UART1_data_receive() {
	return (uint8_t) (USART1->dt);
}

uint8_t UART2_data_receive() {
	return (uint8_t) (USART2->dt);
}

void USART1_IRQHandler(void) {
	/* add user code begin USART1_IRQ 0 */
	if (usart_interrupt_flag_get(USART1, USART_RDBF_FLAG) != RESET) {
		while (usart_flag_get(USART1, USART_RDBF_FLAG) != RESET) {
			usart_data_transmit(USART2, usart_data_receive(USART1));
		}
	}
	/* add user code end USART1_IRQ 0 */
	/* add user code begin USART1_IRQ 1 */

	/* add user code end USART1_IRQ 1 */
}

/**
 * @brief  this function handles USART2 handler.
 * @param  none
 * @retval none
 */
void USART2_IRQHandler(void) {
	/* add user code begin USART2_IRQ 0 */
	if (usart_interrupt_flag_get(USART2, USART_RDBF_FLAG) != RESET) {
		while (usart_flag_get(USART2, USART_RDBF_FLAG) != RESET) {
			usart_data_transmit(USART1, usart_data_receive(USART2));
		}

	}

	//usart_interrupt_enable(USART2, USART_RDBF_INT, FALSE);

	/* add user code end USART2_IRQ 0 */
	/* add user code begin USART2_IRQ 1 */

	/* add user code end USART2_IRQ 1 */
}

void RTCAlarm_IRQHandler(void) {
	/* add user code begin RTCAlarm_IRQ 0 */

	rtc_flag_clear(RTC_TA_INT);
	exint_flag_clear(EXINT_LINE_17);
	dma_channel_enable(DMA1_CHANNEL3, FALSE);

	RTC_Time_Alarm_Reload();
	//wdt_counter_reload();

	/* add user code end RTCAlarm_IRQ 0 */
	/* add user code begin RTCAlarm_IRQ 1 */

	/* add user code end RTCAlarm_IRQ 1 */
}

/* add user code end 4 */
