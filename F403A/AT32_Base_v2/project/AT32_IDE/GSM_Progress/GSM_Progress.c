/*
 * GSM_Progress.c
 *
 *  Created on: 2025 Jan 25
 *      Author: FDany
 */

#include "GSM_Progress.h"

char const AT_command[6] = "AT\r\n";
char const Manufacturer_module[10] = "AT+GMI\r\n";
char const Model_module[10] = "AT+GMM\r\n";
char const Revision_module[10] = "AT+GMR\r\n";
char const CFUN0[16] = "AT+CFUN=0\r\n";
char const CFUN1[16] = "AT+CFUN=1\r\n";
char const CFUN4[16] = "AT+CFUN=4\r\n";
char const ZPAS[16] = "AT+ZPAS?\r\n";
char const ZIPCFG[32] = "AT+ZIPCFG=internet.tele2.ru,,\r\n";
char const ZIPCALL1[16] = "AT+ZIPCALL=1\r\n";
char const ZIPCALL0[16] = "AT+ZIPCALL=0\r\n";
char ZHTTPURL[96] = "AT+ZHTTPURL=https://api.thingspeak.com/update?api_key=Y2DIGCSQALVSOWE4&field1=\0";
char const ZHTTPGET[14] = "AT+ZHTTPGET\r\n";

