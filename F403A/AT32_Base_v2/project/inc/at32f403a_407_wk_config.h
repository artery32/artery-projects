/* add user code begin Header */
/**
  **************************************************************************
  * @file     at32f403a_407_wk_config.h
  * @brief    header file of work bench config
  **************************************************************************
  *                       Copyright notice & Disclaimer
  *
  * The software Board Support Package (BSP) that is made available to
  * download from Artery official website is the copyrighted work of Artery.
  * Artery authorizes customers to use, copy, and distribute the BSP
  * software and its related documentation for the purpose of design and
  * development in conjunction with Artery microcontrollers. Use of the
  * software is governed by this copyright notice and the following disclaimer.
  *
  * THIS SOFTWARE IS PROVIDED ON "AS IS" BASIS WITHOUT WARRANTIES,
  * GUARANTEES OR REPRESENTATIONS OF ANY KIND. ARTERY EXPRESSLY DISCLAIMS,
  * TO THE FULLEST EXTENT PERMITTED BY LAW, ALL EXPRESS, IMPLIED OR
  * STATUTORY OR OTHER WARRANTIES, GUARANTEES OR REPRESENTATIONS,
  * INCLUDING BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.
  *
  **************************************************************************
  */
/* add user code end Header */

/* define to prevent recursive inclusion -----------------------------------*/
#ifndef __AT32F403A_407_WK_CONFIG_H
#define __AT32F403A_407_WK_CONFIG_H

#ifdef __cplusplus
extern "C" {
#endif

/* includes -----------------------------------------------------------------------*/
#include "at32f403a_407.h"

/* private includes -------------------------------------------------------------*/
/* add user code begin private includes */
#include <stdio.h>
#include <string.h>
#include "GSM_Progress.h"
/* add user code end private includes */

/* exported types -------------------------------------------------------------*/
/* add user code begin exported types */

/* add user code end exported types */

/* exported constants --------------------------------------------------------*/
/* add user code begin exported constants */

/* add user code end exported constants */

/* exported macro ------------------------------------------------------------*/
/* add user code begin exported macro */

/* add user code end exported macro */

/* add user code begin dma define */
/* user can only modify the dma define value */
#define DMA1_CHANNEL1_BUFFER_SIZE   0
#define DMA1_CHANNEL1_MEMORY_BASE_ADDR   0
//#define DMA1_CHANNEL1_PERIPHERAL_BASE_ADDR  0

#define DMA1_CHANNEL2_BUFFER_SIZE   0
#define DMA1_CHANNEL2_MEMORY_BASE_ADDR   0
//#define DMA1_CHANNEL2_PERIPHERAL_BASE_ADDR   0

#define DMA1_CHANNEL3_BUFFER_SIZE   0
#define DMA1_CHANNEL3_MEMORY_BASE_ADDR   0
//#define DMA1_CHANNEL3_PERIPHERAL_BASE_ADDR   0

#define DMA1_CHANNEL4_BUFFER_SIZE   0
#define DMA1_CHANNEL4_MEMORY_BASE_ADDR   0
//#define DMA1_CHANNEL4_PERIPHERAL_BASE_ADDR   0

//#define DMA1_CHANNEL5_BUFFER_SIZE   0
//#define DMA1_CHANNEL5_MEMORY_BASE_ADDR   0
//#define DMA1_CHANNEL5_PERIPHERAL_BASE_ADDR   0

//#define DMA1_CHANNEL6_BUFFER_SIZE   0
//#define DMA1_CHANNEL6_MEMORY_BASE_ADDR   0
//#define DMA1_CHANNEL6_PERIPHERAL_BASE_ADDR   0

//#define DMA1_CHANNEL7_BUFFER_SIZE   0
//#define DMA1_CHANNEL7_MEMORY_BASE_ADDR   0
//#define DMA1_CHANNEL7_PERIPHERAL_BASE_ADDR   0

//#define DMA2_CHANNEL1_BUFFER_SIZE   0
//#define DMA2_CHANNEL1_MEMORY_BASE_ADDR   0
//#define DMA2_CHANNEL1_PERIPHERAL_BASE_ADDR   0

//#define DMA2_CHANNEL2_BUFFER_SIZE   0
//#define DMA2_CHANNEL2_MEMORY_BASE_ADDR   0
//#define DMA2_CHANNEL2_PERIPHERAL_BASE_ADDR   0

//#define DMA2_CHANNEL3_BUFFER_SIZE   0
//#define DMA2_CHANNEL3_MEMORY_BASE_ADDR   0
//#define DMA2_CHANNEL3_PERIPHERAL_BASE_ADDR   0

//#define DMA2_CHANNEL4_BUFFER_SIZE   0
//#define DMA2_CHANNEL4_MEMORY_BASE_ADDR   0
//#define DMA2_CHANNEL4_PERIPHERAL_BASE_ADDR   0

//#define DMA2_CHANNEL5_BUFFER_SIZE   0
//#define DMA2_CHANNEL5_MEMORY_BASE_ADDR   0
//#define DMA2_CHANNEL5_PERIPHERAL_BASE_ADDR   0

//#define DMA2_CHANNEL6_BUFFER_SIZE   0
//#define DMA2_CHANNEL6_MEMORY_BASE_ADDR   0
//#define DMA2_CHANNEL6_PERIPHERAL_BASE_ADDR   0

//#define DMA2_CHANNEL7_BUFFER_SIZE   0
//#define DMA2_CHANNEL7_MEMORY_BASE_ADDR   0
//#define DMA2_CHANNEL7_PERIPHERAL_BASE_ADDR   0
/* add user code end dma define */

/* Private defines -------------------------------------------------------------*/
#define LED5_PIN    GPIO_PINS_13
#define LED5_GPIO_PORT    GPIOC
#define LED6_PIN    GPIO_PINS_14
#define LED6_GPIO_PORT    GPIOC
#define Bat_voltage_PIN    GPIO_PINS_1
#define Bat_voltage_GPIO_PORT    GPIOA
#define NSS_SPI1_PIN    GPIO_PINS_4
#define NSS_SPI1_GPIO_PORT    GPIOA
#define BUSY_LoRa900_PIN    GPIO_PINS_4
#define BUSY_LoRa900_GPIO_PORT    GPIOC
#define TXEN_LoRa900_PIN    GPIO_PINS_5
#define TXEN_LoRa900_GPIO_PORT    GPIOC
#define RXEN_LoRa900_PIN    GPIO_PINS_0
#define RXEN_LoRa900_GPIO_PORT    GPIOB
#define NRST_LoRa900_PIN    GPIO_PINS_1
#define NRST_LoRa900_GPIO_PORT    GPIOB
#define JDY41_Set_PIN    GPIO_PINS_2
#define JDY41_Set_GPIO_PORT    GPIOB
#define NSS_SPI2_PIN    GPIO_PINS_12
#define NSS_SPI2_GPIO_PORT    GPIOB
#define RST_LoRa433_PIN    GPIO_PINS_6
#define RST_LoRa433_GPIO_PORT    GPIOC
#define IO0_LoRa433_PIN    GPIO_PINS_7
#define IO0_LoRa433_GPIO_PORT    GPIOC
#define LoRa900T22D_M0_PIN    GPIO_PINS_11
#define LoRa900T22D_M0_GPIO_PORT    GPIOA
#define LoRa900T22D_M1_PIN    GPIO_PINS_12
#define LoRa900T22D_M1_GPIO_PORT    GPIOA
#define LoRa900T22D_AUX_PIN    GPIO_PINS_15
#define LoRa900T22D_AUX_GPIO_PORT    GPIOA
#define Sleep_GSM_PIN    GPIO_PINS_5
#define Sleep_GSM_GPIO_PORT    GPIOB
#define Reset_GSM_PIN    GPIO_PINS_6
#define Reset_GSM_GPIO_PORT    GPIOB

/* exported functions ------------------------------------------------------- */
  /* system clock config. */
  void wk_system_clock_config(void);

  /* config periph clock. */
  void wk_periph_clock_config(void);

  /* init debug function. */
  void wk_debug_config(void);

  /* nvic config. */
  void wk_nvic_config(void);

  /* init gpio function. */
  void wk_gpio_config(void);

  /* init exint function. */
  void wk_exint_config(void);

  /* init adc1 function. */
  void wk_adc1_init(void);

  /* init rtc function. */
  void wk_rtc_init(void);

  /* init i2c2 function. */
  void wk_i2c2_init(void);

  /* init pwc function. */
  void wk_pwc_init(void);

  /* init spi1 function. */
  void wk_spi1_init(void);

  /* init spi2 function. */
  void wk_spi2_init(void);

  /* init usart1 function. */
  void wk_usart1_init(void);

  /* init usart2 function. */
  void wk_usart2_init(void);

  /* init uart4 function. */
  void wk_uart4_init(void);

  /* init uart5 function. */
  void wk_uart5_init(void);

  /* init wdt function. */
  void wk_wdt_init(void);

  /* init dma1 channel1 */
  void wk_dma1_channel1_init(void);

  /* init dma1 channel2 */
  void wk_dma1_channel2_init(void);

  /* init dma1 channel3 */
  void wk_dma1_channel3_init(void);

  /* init dma1 channel4 */
  void wk_dma1_channel4_init(void);

  /* config dma channel transfer parameter */
  /* user need to modify parameters memory_base_addr and buffer_size */
  void wk_dma_channel_config(dma_channel_type* dmax_channely, uint32_t peripheral_base_addr, uint32_t memory_base_addr, uint16_t buffer_size);

/* add user code begin exported functions */
  void AT_Transmit(char* command);
  void Usart2_Send_symbol(uint8_t data);
  void Usart1_Send_symbol(uint8_t data);
  uint8_t UART1_data_receive();
  uint8_t UART2_data_receive();
  void UART1_Transmit(char* text);
  void RTC_Time_Alarm_Reload(void);
  void GSM_Sleep(void);
  void GSM_WakeUp(void);
  void Tranciever_onNetwork(void);
  void UART_clear_buffer(usart_type* usart_x);
  void Sleep_Mode_pulling(void);
  void Enter_toSleep(void);
/* add user code end exported functions */

#ifdef __cplusplus
}
#endif

#endif
