/* add user code begin Header */
/**
  **************************************************************************
  * @file     main.c
  * @brief    main program
  **************************************************************************
  *                       Copyright notice & Disclaimer
  *
  * The software Board Support Package (BSP) that is made available to
  * download from Artery official website is the copyrighted work of Artery.
  * Artery authorizes customers to use, copy, and distribute the BSP
  * software and its related documentation for the purpose of design and
  * development in conjunction with Artery microcontrollers. Use of the
  * software is governed by this copyright notice and the following disclaimer.
  *
  * THIS SOFTWARE IS PROVIDED ON "AS IS" BASIS WITHOUT WARRANTIES,
  * GUARANTEES OR REPRESENTATIONS OF ANY KIND. ARTERY EXPRESSLY DISCLAIMS,
  * TO THE FULLEST EXTENT PERMITTED BY LAW, ALL EXPRESS, IMPLIED OR
  * STATUTORY OR OTHER WARRANTIES, GUARANTEES OR REPRESENTATIONS,
  * INCLUDING BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.
  *
  **************************************************************************
  */
/* add user code end Header */

/* Includes ------------------------------------------------------------------*/
#include "at32f403a_407_wk_config.h"

/* private includes ----------------------------------------------------------*/
/* add user code begin private includes */

/* add user code end private includes */

/* private typedef -----------------------------------------------------------*/
/* add user code begin private typedef */

/* add user code end private typedef */

/* private define ------------------------------------------------------------*/
/* add user code begin private define */

/* add user code end private define */

/* private macro -------------------------------------------------------------*/
/* add user code begin private macro */

/* add user code end private macro */

/* private variables ---------------------------------------------------------*/
/* add user code begin private variables */

/* add user code end private variables */

/* private function prototypes --------------------------------------------*/
/* add user code begin function prototypes */

/* add user code end function prototypes */

/* private user code ---------------------------------------------------------*/
/* add user code begin 0 */

/* add user code end 0 */

/**
  * @brief main function.
  * @param  none
  * @retval none
  */
int main(void)
{
  /* add user code begin 1 */

  /* add user code end 1 */

  /* system clock config. */
  wk_system_clock_config();

  /* config periph clock. */
  wk_periph_clock_config();

  /* init debug function. */
  wk_debug_config();

  /* nvic config. */
  wk_nvic_config();

  /* init usart1 function. */
  wk_usart1_init();

  /* init usart2 function. */
  wk_usart2_init();

  /* init usart3 function. */
  wk_usart3_init();

  /* init uart4 function. */
  wk_uart4_init();

  /* init usart6 function. */
  wk_usart6_init();

  /* init i2c1 function. */
  wk_i2c1_init();

  /* init gpio function. */
  wk_gpio_config();

  /* add user code begin 2 */
  	delay_init();
  	uint16_t data1 = 0xAA;
  	uint16_t code = 0xBB;
  	uint16_t codeCtrl = 0xAB;
  	uint16_t data2 = 0;
    uint16_t data3 = 0;
    uint16_t data4 = 0;
  	uint16_t data5 = 0;
    uint16_t Crc = 0;
    delay_ms(1000);
    //gpio_bits_write(GPIOB, GPIO_PINS_13, TRUE);
    gpio_bits_write(GPIOB, GPIO_PINS_12, TRUE);
    gpio_bits_write(GPIOA, GPIO_PINS_5, TRUE);
    gpio_bits_write(GPIOA, GPIO_PINS_8, TRUE);
    delay_ms(1000);

  /* add user code end 2 */

  while(1)
  {
    /* add user code begin 3 */
	  if ((usart_data_receive (USART3)) == code){
	  		  while ((usart_data_receive (USART3)) == code) {

	  		  }
	  		  delay_ms(3);
	  		  data2 = usart_data_receive (USART3);
	  		  delay_ms(3);
	  		  data3 = usart_data_receive (USART3);
	  		  delay_ms(3);
	  		  data4 = usart_data_receive (USART3);
	  		  delay_ms(3);
	  		  data5 = usart_data_receive (USART3);
	  		  delay_ms(500);

	  		  Crc = ((data2 + data3 + data4)/5);

	  		  if (Crc == data5) {
	  			  gpio_bits_write(GPIOB, GPIO_PINS_13, TRUE);
	  			  usart_data_transmit (USART1, codeCtrl);
	  			  delay_ms(500);
	  			  usart_data_transmit (USART1, data2);
	  			  delay_ms(500);
	  			  //usart_data_transmit (USART1, data3);
	  			  delay_ms(500);
	  			  //sart_data_transmit (USART1, data4);
	  			  delay_ms(500);
	  			  gpio_bits_write(GPIOB, GPIO_PINS_13, FALSE);
	  		  }
	  	  }
	  //gpio_bits_write(GPIOB, GPIO_PINS_13, FALSE);
	  data5 = 0;
	  Crc = 0;
    /* add user code end 3 */
  }
}
