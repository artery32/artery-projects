/* add user code begin Header */
/**
  **************************************************************************
  * @file     wk_ertc.c
  * @brief    work bench config program
  **************************************************************************
  *                       Copyright notice & Disclaimer
  *
  * The software Board Support Package (BSP) that is made available to
  * download from Artery official website is the copyrighted work of Artery.
  * Artery authorizes customers to use, copy, and distribute the BSP
  * software and its related documentation for the purpose of design and
  * development in conjunction with Artery microcontrollers. Use of the
  * software is governed by this copyright notice and the following disclaimer.
  *
  * THIS SOFTWARE IS PROVIDED ON "AS IS" BASIS WITHOUT WARRANTIES,
  * GUARANTEES OR REPRESENTATIONS OF ANY KIND. ARTERY EXPRESSLY DISCLAIMS,
  * TO THE FULLEST EXTENT PERMITTED BY LAW, ALL EXPRESS, IMPLIED OR
  * STATUTORY OR OTHER WARRANTIES, GUARANTEES OR REPRESENTATIONS,
  * INCLUDING BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.
  *
  **************************************************************************
  */
/* add user code end Header */

/* Includes ------------------------------------------------------------------*/
#include "wk_ertc.h"

/* add user code begin 0 */

/* add user code end 0 */

/**
  * @brief  init ertc function.
  * @param  none
  * @retval none
  */
void wk_ertc_init(void)
{
  /* add user code begin ertc_init 0 */

  /* add user code end ertc_init 0 */

  exint_init_type exint_init_struct;

  /* add user code begin ertc_init 1 */

  /* add user code end ertc_init 1 */

  pwc_battery_powered_domain_access(TRUE);

  crm_ertc_clock_select(CRM_ERTC_CLOCK_LICK);
  crm_ertc_clock_enable(TRUE);
  ertc_reset();
  ertc_wait_update();
  ertc_divider_set(127, 255);
  ertc_hour_mode_set(ERTC_HOUR_MODE_24);

  /* configure Wakeup timer */
  ertc_wakeup_clock_set(ERTC_WAT_CLK_CK_B_17BITS);
  ertc_wakeup_counter_set(0);

  exint_default_para_init(&exint_init_struct);
  exint_init_struct.line_enable   = TRUE;
  exint_init_struct.line_mode     = EXINT_LINE_INTERRUPUT;
  exint_init_struct.line_select   = EXINT_LINE_20;
  exint_init_struct.line_polarity = EXINT_TRIGGER_RISING_EDGE;
  exint_init(&exint_init_struct);
  /**
   * Users need to configure this interrupt functions according to the actual application.
   * 1. Call the below function to enable this  interrupt.
   *      --ertc_interrupt_enable(ERTC_WAT_INT, TRUE)
   * 2. Add the user's interrupt handler code into the below function in the at32l021_int.c file.
   *      --void ERTC_WKUP_IRQHandler (void)
   */

  /* add user code begin ertc_init wakeup_timer */

  /* add user code end ertc_init wakeup_timer */

  ertc_wakeup_enable(TRUE);  

  /* add user code begin ertc_init 2 */

  /* add user code end ertc_init 2 */
}

/* add user code begin 1 */

/* add user code end 1 */
